package main

import (
	"encoding/gob"
	"fmt"
	"flag"
	"os"
	"reflect"
)

var filename = flag.String("file",fmt.Sprint(os.TempDir(),"/fa.gob"),"fichier temporaire à utiliser pour le gob")

//le conteneur permet de transmettre tout type de valeurs à l'interieur
//gob ne peut pas recevoir dans un interface{}, d'où la nécessité d'un wrapper struct
type Conteneur struct {
	Valeurs []interface{}
}

//Exemple avec deux types personnalisés
type MonInt int
type Autre int

func main() {
	//récupération du fichier pour l'enregistrement du gob
	flag.Parse()
	//création des variables pour l'exemple
	var a MonInt
	a = 5
	var b Autre
	b = 10
	//registration 
	gob.Register(a)
	gob.Register(b)

	//création et remplissage du conteneur
	var cEnvoi Conteneur
	cEnvoi.Valeurs = append(cEnvoi.Valeurs, a)
	cEnvoi.Valeurs = append(cEnvoi.Valeurs, b)

	//ouverture d'un fichier pour enregistrer le gob
	f, err := os.Create(*filename)
	if err != nil {
		fmt.Println(err)
	}

	//encodage du conteneur
	enc := gob.NewEncoder(f)
	enc.Encode(cEnvoi)
	f.Close()

	//création d'un conteneur pour la réception
	var cReception Conteneur

	//décodage depuis le fichier
	f2, err := os.Open(*filename)
	if err != nil {

		fmt.Println(err)
	}
	dec := gob.NewDecoder(f2)
	err = dec.Decode(&cReception)
	if err != nil {
		fmt.Println("Décodage : ", err)
	}

	//lecture du slice des valeurs reçues
	for _, val := range cReception.Valeurs {
		switch val.(type) { // on met tous les types qu'on a pu transmettre.
		//le default utilise le package reflect pour obtenir le type
		case int:
			fmt.Println("on a décodé un int, sa valeur est", val.(int))
		case MonInt:
			fmt.Println("on a décodé un MonInt, sa valeur est", val.(MonInt))
		default:
			fmt.Println("Type inconnu ! Par reflection, son type est : ", reflect.TypeOf(val), "sa valeur est ", reflect.ValueOf(val).Int())

		}

	}
}
